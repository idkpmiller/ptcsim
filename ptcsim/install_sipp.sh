#!/bin/bash

##############################
# by Paul Miller
# installation script for 
# SIPp with a focus on Raspberry Pi
# Wheezy installations.
# Limitations:
# ------------
# hard-coded for specific versions 
# hard-coded for SSL with pcapplay - no support options  
#
##############################
# Version:
# --------
# 0.1 initial version
# 0.2 hard-coded for SIPp version 3.3.990
# 0.3 added support for 3.4.1 (unstable release) as it has considerable bug fixes
# 0.4 updated SIpp 3.4 installs to no longer use v3.3990
# 0.5 added the --with-pcap --with-openssl options to the configure process
#     modified the 3.4.1 function to move the extracted directory to /opt like the 3.4.0 function
# 0.6 Removed dialog logic
#     Correct tput requirements
#     corrected version number
#     tidied up messages and spelling
# 0.7 add support for current MASTER branch which provides the very latest updates
# 0.8 added some error checking and a new _rmdir function
#  
##############################

bold=`tput bold`
normal=`tput sgr0`

SELF=$(basename $0)
SCRIPT_NAME="$0"
ARGS="$@"
VERSION="0.7"

APPPKG="libpcap0.8* openssl build-essential libssl-dev libncurses5-dev subversion"

#---------------------
# Functions
#---------------------
function pTitle () {
#Requires:
#  bold=`tput bold`
#  normal=`tput sgr0`
# Title is passed as argument to the function.
TITLE=$1
  echo "================================================================================"
  echo ${bold}$TITLE${normal}
  echo "================================================================================"
}

function func_chkRoot() {
echo "================================================================================"
echo "Checking for root Privileges:"
echo "================================================================================"
logger "$0 v${VERSION}, entered OS ENVIRONMENT CHECKS" 
(( `id -u` )) && echo "Must be ran as root, try prefixing with sudo." && exit 1
}

function func_chkInternet () {
/usr/bin/wget -q --tries=10 --timeout=5 http://www.google.com -O /tmp/index.google &> /dev/null
if [ ! -s /tmp/index.google ];then
        echo "No Internet connection. Exiting."
        /bin/rm /tmp/index.google
        exit 1
else
        echo "Internet connection is functioning"
        /bin/rm /tmp/index.google
fi
}

function _rmdir () {
if [ -d $1 ];then
        /bin/rm -r $1
        pTitle "Removal of directory $1 - successful"		
  else
        pTitle "Removal of directory $1 - skipped, directory not found"
fi
}


function func_updSystem() {
echo 'updating system, please Wait.'
echo ""
  apt-get -y -qq update
  
  #Check if user want to upgrade now.
  while true; do
    read -p "Would you like to upgrade the system now? (Y/N)" answer
    case $answer in
        [Yy] ) 
			echo "Upgrading ${bold}please wait..${normal}";
			apt-get -y upgrade;
			apt-get -y autoremove;
			break;;
        [Nn] ) echo "NO, selected so continuing"; break;;
        * ) echo "Please answer Y or N.";;
    esac
done
#func_updSystem
echo "================================================================================"
echo ""    
}

function func_InstPackages(){
# call finction with list of packages to be installed as the argument"
# e.g. 
# MusicPKG="mp3 sox"
# func_InstPackages MusicPKG
##########################
PACKAGES="${!1}"

for pkg in $PACKAGES; do
  if apt-get -y -qq install $pkg; then
    echo "${bold}Successfully installed $pkg ${normal}"
  else
    echo "${bold}Error installing $pkg ${normal}"
	exit 1
  fi
done
echo "${bold}Package $1 installation complete. ${normal}"
echo "================================================================================"
echo ""
}

function func_InstSIPp-3.4-STABLE () {
apt-get -y remove sip-tester

func_InstPackages APPPKG

# remove any previous attempts
_rmdir /usr/src/sipp-3.4.0
_rmdir /usr/src/sipp

cd /usr/src
rm -rf sipp-3.*

wget "https://github.com/SIPp/sipp/archive/3.4.0.tar.gz" -O /usr/src/sipp.tar.gz
gunzip sipp.tar.gz
tar -xvf sipp.tar
rm -f sipp.tar
rm -f sipp.tar.gz
cd sipp-3.4.0

./configure --with-pcap --with-openssl
make
if [ ! "$?" = "0" ]; then
	echo "Compilation issue" 1>&2
    exit 1
fi

cp sipp /usr/bin/sipp

# delete destination directory and copy new version to /opt 
_rmdir /opt/sipp-3.4.0

mv /usr/src/sipp-3.4.0 /opt/sipp-3.4.0

sipp -v
} 

function func_InstSIPp-UNSTABLE () {
VER="3.4.1"
FULLunstableFN="v${VER}.tar.gz"
DIRunstableFN="sipp-${VER}"
apt-get -y remove sip-tester

func_InstPackages APPPKG

# remove any previous attempts
_rmdir /usr/src/sipp-${VER}
_rmdir /usr/src/sipp

cd /usr/src
wget "https://github.com/SIPp/sipp/archive/$FULLunstableFN" -O /usr/src/sipp.tar.gz
gunzip sipp.tar.gz
tar -xvf sipp.tar
rm -f sipp.tar
rm -f sipp.tar.gz

cd $DIRunstableFN

./configure --with-pcap --with-openssl
make
if [ ! "$?" = "0" ]; then
	echo "Compilation issue" 1>&2
    exit 1
fi

cp sipp /usr/bin/sipp

# delete destination directory and copy new version to /opt 
_rmdir /opt/sipp-${VER}

mv /usr/src/sipp-${VER} /opt/sipp-${VER}

sipp -v
}

function func_InstSIPp-MASTER () {
apt-get -y remove sip-tester

func_InstPackages APPPKG

# remove any previous attempts
_rmdir /usr/src/sipp-master
_rmdir /usr/src/sipp

cd /usr/src
wget "https://github.com/SIPp/sipp/archive/master.tar.gz" -O /usr/src/sipp.tar.gz
gunzip sipp.tar.gz
tar -xvf sipp.tar
rm -f sipp.tar
rm -f sipp.tar.gz

cd sipp-master

./configure --with-pcap --with-openssl

# due to issues with unit test compilation this is changed to make sipp
make sipp
if [ ! "$?" = "0" ]; then
	echo "Compilation issue" 1>&2
    exit 1
fi

#copy to a binary path directory
cp sipp /usr/bin/sipp

# delete destination directory and copy new version to /opt 
_rmdir /opt/sipp-master
mv /usr/src/sipp-master /opt/sipp-master

sipp -v
}

function func_SIPpInstChoose () {
#Prompts User to select which SIPp version to install
logger "$0 Entered func_SIPpInstChoose"
    echo ""
	pTitle "Which SIPp version to install:"
	echo "1 - Stable_3.4"
	echo "2 - Unstable_3.4.1"
	echo "3 - Master"
	echo "N - None"
	while true; do
		read -p "Select a SIPp version to be installed?" answer
		case $answer in
			1 ) SIPPVER="Stable_3.4"; func_InstSIPp-3.4-STABLE; break;;
			2 ) SIPPVER="Unstable_3.4.1"; func_InstSIPp-UNSTABLE; break;;
			3 ) SIPPVER="Master"; func_InstSIPp-MASTER; break;;
			[Nn] ) SIPPVER="NONE" ; break;;
			* ) echo "Please select a valid option.";;
		esac
	done
# End func_SIPpInstChoose
} 

#---------------------
# OS ENVIRONMENT CHECKS
#---------------------
clear
func_chkRoot
func_chkInternet
func_updSystem

#---------------------
# APPLICATION INSTALL
#---------------------
func_SIPpInstChoose


