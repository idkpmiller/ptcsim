#!/bin/bash
#Paul's DSCP marking iptables
# Reduced to only marking outgoing VOIP traffic


# Flush the tables first
#iptables -F
#iptables -t mangle -F

# RealTime Group (46)
#=============================
# RTP & RTCP
#should be marked by application so audio and video differentiation can be obtained
iptables -t mangle -A OUTPUT -p udp -m udp --sport 6000:6099 -j DSCP --set-dscp-class ef # mark RTP& RTCP packets with EF


# Signalling Group (24)
#=============================
# SIP
# Should be set by application, only to ne used when app not capable.
iptables -t mangle -A OUTPUT -p udp -m udp --dport 5060 -j DSCP --set-dscp-class cs3 # mark SIPGate SIP UDP packets
iptables -t mangle -A OUTPUT -p tcp --dport 5060 -j DSCP --set-dscp-class cs3 # mark SIP TCP packets with CS3

# commit the changes to file
iptables-save > /etc/iptables.rules
